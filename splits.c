#include "auto_qmail.h"
#include "auto_split.h"
#include "auto_uids.h"
#include "error.h"
#include "fmt.h"
#include "fifo.h"
#include "strerr.h"

#define FATAL "install: fatal: "

void d(home,subdir,uid,gid,mode)
char *home;
char *subdir;
int uid;
int gid;
int mode;
{
  if (chdir(home) == -1)
    strerr_die4sys(111,FATAL,"unable to switch to ",home,": ");
  if (mkdir(subdir,0700) == -1)
    if (errno != error_exist)
      strerr_die6sys(111,FATAL,"unable to mkdir ",home,"/",subdir,": ");
  if (chown(subdir,uid,gid) == -1)
    strerr_die6sys(111,FATAL,"unable to chown ",home,"/",subdir,": ");
  if (chmod(subdir,mode) == -1)
    strerr_die6sys(111,FATAL,"unable to chmod ",home,"/",subdir,": ");
}

char buf[100 + FMT_ULONG];

void dsplit(base,uid,mode)
char *base; /* must be under 100 bytes */
int uid;
int mode;
{
  char *x;
  unsigned long i;

  d(auto_qmail,base,uid,auto_gidq,mode);

  for (i = 0;i < auto_split;++i) {
    x = buf;
    x += fmt_str(x,base);
    x += fmt_str(x,"/");
    x += fmt_ulong(x,i);
    *x = 0;

    d(auto_qmail,buf,uid,auto_gidq,mode);
  }
}

int main(void)
{
  dsplit("queue/mess",auto_uidq,0750);
  dsplit("queue/info",auto_uids,0700);
  dsplit("queue/local",auto_uids,0700);
  dsplit("queue/remote",auto_uids,0700);
  return 0;
}
