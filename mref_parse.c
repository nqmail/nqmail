#include "mref.h"

int mref_parse(const char *m, char **f, size_t *fn, char **t, size_t *tn) {
    if (*m++ != '0') return 0;
    if (*m++ != 'L') return 0;
    if (*m++ != ',') return 0;
    *f = m;
    while (*m != '\0' && *m != ',') ++m;
    if (*m == '\0') return 0;
    *fn = m - *f;
    ++m; *t = m;
    while (*m != '\0' && *m != ',') ++m;
    if (*m == '\0') return 0;
    *tn = m - *t;
    return 1;
}
