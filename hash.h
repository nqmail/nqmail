#include "stralloc.h"

struct hash_table {
	struct hash_item **hashes;
	unsigned int n;
};

int hash_init(struct hash_table *, unsigned int);
int hash_insert(struct hash_table *, stralloc *, stralloc *);
int hash_lookup(struct hash_table *, stralloc *, stralloc *);
int hash_lookups(struct hash_table *, const char *, stralloc *);
