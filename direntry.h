#ifndef DIRENTRY_H
#define DIRENTRY_H

#if HASdrent
#include <sys/types.h>
#include <dirent.h>
#define direntry struct dirent
#else
#include <sys/types.h>
#include <sys/dir.h>
#define direntry struct direct
#endif

#endif
