#ifndef FORK_H
#define FORK_H

extern int fork();

#if HASVFORK
extern int vfork();
#else
#define vfork fork
#endif

#endif
