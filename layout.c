#include <ctype.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <grp.h>
#include <pwd.h>

#include "buffer.h"
#include "error.h"
#include "fifo.h"
#include "hash.h"
#include "open.h"
#include "sgetopt.h"
#include "str.h"
#include "stralloc.h"
#include "strerr.h"

static const char error[] = "layout: ";
static const char fatal[] = "layout: fatal: ";
static const char warning[] = "layout: warning: ";
static const char usage[] = "layout: usage: layout <layout-file>";

static void nomem(void) {
	strerr_die2sys(1, fatal, "out of memory");
}

struct hash_table ht;

static char *file, *macro;
static int install;

static void echo(const char *fn, const char *text) {
	int fd;
	ssize_t w;
	unsigned int len;

	fd = open_trunc(fn);
	if (fd == -1)
		strerr_die4sys(1, fatal, "cannot open `",
			       fn, "' for output: ");

	len = str_len(text);
	while (len) {
		w = write(fd, text, len);
		if (w == -1) {
			close(fd);
			strerr_die4sys(1, fatal, "cannot write `", fn, "': ");
		}
		text += w;
		len -= w;
	}

	if (close(fd) == -1)
		strerr_die4sys(1, fatal, "cannot close `", fn, "': ");
}

static void create(const char *var, const char *path) {
	unsigned int i;
	stralloc base = { 0 }, define = { 0 }, text = { 0 },  fn = { 0 };

	if (!stralloc_copys(&base, "auto_")) nomem();
	if (!stralloc_cats(&base, var)) nomem();

	if (!stralloc_copy(&fn, &base)) nomem();
	if (!stralloc_cats(&fn, ".c")) nomem();
	if (!stralloc_0(&fn)) nomem();

	if (!stralloc_copys(&text, "const char ")) nomem();
	if (!stralloc_cat(&text, &base)) nomem();
	if (!stralloc_cats(&text, "[] = \"")) nomem();
	if (!stralloc_cats(&text, path)) nomem();
	if (!stralloc_cats(&text, "\";\n")) nomem();
	if (!stralloc_0(&text)) nomem();

	echo(fn.s, text.s);

	if (!stralloc_copy(&fn, &base)) nomem();
	if (!stralloc_cats(&fn, ".h")) nomem();
	if (!stralloc_0(&fn)) nomem();

	for (i = 0; i < base.len; ++i) {
		char c = base.s[i];
		if (c == '.') c = '_';
		if (c >= 'a' && c <= 'z') c += 'A' - 'a';
		if (!stralloc_append(&define, &c)) nomem();
	}
	if (!stralloc_cats(&define, "_H")) nomem();

	if (!stralloc_copys(&text, "#ifndef ")) nomem();
	if (!stralloc_cat(&text, &define)) nomem();
	if (!stralloc_cats(&text, "\n#define ")) nomem();
	if (!stralloc_cat(&text, &define)) nomem();
	if (!stralloc_cats(&text, "\nextern const char ")) nomem();
	if (!stralloc_cat(&text, &base)) nomem();
	if (!stralloc_cats(&text, "[];\n#endif\n")) nomem();
	if (!stralloc_0(&text)) nomem();

	echo(fn.s, text.s);

}

static mode_t mode_get(const char *f) {
	struct stat st;

	if (stat(f, &st) == -1)
		strerr_die4sys(1, fatal, "cannot stat `", f, "': ");
	return st.st_mode & 07777;
}

static mode_t octal(stralloc *m) {
    int i;
    mode_t t = 0;
    for (i = 0; i < m->len; ++i) {
        if (m->s[i] < '0' || m->s[i] > '7')
            strerr_die2x(1, fatal, "bad octal digit");
        t = t * 8 + m->s[i] - '0';
    }
    return t;
}

static gid_t uid_get(stralloc *u) {
    struct passwd *p;
    if (!stralloc_0(u)) nomem();
    p = getpwnam(u->s);
    if (!p) strerr_die4x(1, fatal, "no such user `", u->s, "'");
    return p->pw_uid;
}

static gid_t gid_get(stralloc *gs) {
    struct group *g;
    if (!stralloc_0(gs)) nomem();
    g = getgrnam(gs->s);
    if (!g) strerr_die4x(1, fatal, "no such user `", gs->s, "'");
    return g->gr_gid;
}

static void perm_set(const char *f, const char *from) {
    gid_t gid;
    mode_t mode;
    stralloc modes = { 0 }, user = { 0 }, group = { 0 };
    uid_t uid;

    if (!hash_lookups(&ht, "user", &user)) nomem();
    if (!hash_lookups(&ht, "group", &group)) nomem();
    if ((user.s > 0) != (group.s > 0))
        strerr_die2x(1, fatal, "must specify user and group");
    if (user.s > 0) {
        uid = uid_get(&user);
        gid = gid_get(&group);
        if (chown(f, uid, gid) == -1)
            strerr_die4sys(1, fatal, "cannot chown `", f, "': ");
    }

    if (!hash_lookups(&ht, "mode", &modes)) nomem();
    mode = modes.s ? octal(&modes) : mode_get(from);
    if (chmod(f, mode) == -1)
        strerr_die4sys(1, fatal, "cannot chmod `", f, "': ");
}

/* Warning: this function always strips the last path component from
   its argument: add a trailing slash if required. */
static void make_dir(const char *d) {
    int r;
    unsigned int i, slash = 0;
    stralloc dir = { 0 };

    if (!stralloc_copys(&dir, d)) nomem();
    for (i = 0; i < dir.len; ++i)
        if (dir.s[i] == '/')
            slash = i;
    if (slash == 0)
        strerr_die2x(1, fatal, "cannot create root directory!");
    dir.s[slash] = '\0';
    r = mkdir(dir.s, 0755);
    if (r == -1 && errno == error_noent) {
        make_dir(dir.s);
        r = mkdir(dir.s, 0755);
    }
    if (r == -1)
        if (errno != error_exist)
            strerr_die4sys(1, fatal, "cannot make directory `", dir.s, "': ");
    /* If this doesn't do what you want, you may need to explicitly create
     * parent directories. */
    perm_set(dir.s, ".");
}

static void copy(const char *from, const char *to) {
    static char bigbuf[32768];
    int in, out;
    ssize_t r;
    stralloc tmp = { 0 };

    if (!stralloc_copys(&tmp, to)) nomem();
    if (!stralloc_cats(&tmp, ".tmp")) nomem(); /* XXX */
    if (!stralloc_0(&tmp)) nomem();
    out = open_trunc(tmp.s);
    if (out == -1 && errno == error_noent) {
        make_dir(tmp.s);
        out = open_trunc(tmp.s);
    }
    if (out == -1)
        strerr_die4sys(1, fatal,
                "cannot open `", tmp.s, "' for writing: ");

    in = open_read(from);
    if (in == -1)
        strerr_die4sys(1, fatal,
                "cannot open `", from, "' for input: ");

    while ((r = read(in, bigbuf, sizeof bigbuf)) > 0) {
        ssize_t w;

        while (r) {
            w = write(out, bigbuf, r);
            if (w == -1) {
                if (errno == error_intr) continue;
                strerr_die4sys(1, fatal,
                        "cannot write `", tmp.s, "': ");
            }
            r -= w;
        }
    }
    if (r == -1)
        strerr_die4sys(1, fatal, "cannot read `", from, "': ");
    if (close(out) == -1)
        strerr_die4sys(1, fatal, "cannot close `", tmp.s, "': ");
    close(in);

    perm_set(tmp.s, from);

    if (rename(tmp.s, to) == -1)
        strerr_die6sys(1, fatal, "cannot rename `", tmp.s, "' to `",
                to, "': ");
}

static void pipe_create(const char *p) {
    if (fifo_make(p, 0644) == -1)
        if (errno != error_exist)
            strerr_die4sys(1, fatal, "cannot make fifo `", p, "': ");
    perm_set(p, "."); /* XXX - but is there a file that is certain to exist? */
}

static int gotit = 0;
	
static void process(stralloc *target) {
    unsigned int base, s;

    base = 0;
    for (s = 0; s < target->len; ++s) 
        if (target->s[s] == '/')
            base = s + 1;

    if (!stralloc_0(target)) nomem();
    if (file) {
        if (str_equal(file, target->s + base)) {
            gotit = 1;
            create(target->s + base, target->s);
        }
        return;
    }
    if (!install) return;
    if (target->s[base] == '\0')
        make_dir(target->s);
    else if (target->s[target->len - 2] == '|') {
        target->len -= 2;
        stralloc_0(target);
        pipe_create(target->s);
    } else
        copy(target->s + base, target->s);
}

static void parse(stralloc *raw) {
	unsigned int i;
	int assign = -1, comment = -1, ref = -1;
	stralloc name = { 0 }, value = { 0 }, cooked = { 0 };

	for (i = 0; i < raw->len; ++i) {
		switch (raw->s[i]) {
		case '#':
			comment = i;
			break;
		case '=':
			assign = i;
			break;
		case '$':
			ref = i;
			break;
		}
	}
 
	if (comment >= 0) raw->len = comment;

	if (raw->len == 0) return;

	if (ref >= 0) {
		stralloc_copyb(&cooked, raw->s, ref);

		for (i = ref + 1; i < raw->len && 
			     isalnum(raw->s[i]); ++i) {
			stralloc_catb(&name, raw->s + i, 1);
		}
			 
		value.len = 0;
		if (!hash_lookup(&ht, &name, &value)) nomem();
		if (!value.len) {
			if (!stralloc_0(&name)) nomem();
			strerr_die4x(1, fatal,
				     "macro `", name.s, "' not defined");
		}
		if (!stralloc_cat(&cooked, &value)) nomem();
		if (!stralloc_catb(&cooked, raw->s + i, raw->len - i)) nomem();
	} else {
		stralloc_copy(&cooked, raw);
	}

	if (assign >= 0) {
		stralloc_copyb(&name, cooked.s, assign);
		stralloc_copyb(&value, cooked.s + assign + 1,
			       cooked.len - (assign + 1));
		if (!hash_insert(&ht, &name, &value)) nomem();
		if (macro) {
			if (!stralloc_0(&name)) nomem();
			if (!stralloc_0(&value)) nomem();
			if (str_equal(macro, name.s)) {
				gotit = 1;
				create(macro, value.s);
			}
		}
		return;
	}

	if (!macro)
		process(&cooked);
}

int main(int argc, char **argv) {
	char buf[256];
	int fd;
	int opt;
	buffer in;
	char ch;
	stralloc line = { 0 };

	while ((opt = getopt(argc, argv, "f:im:")) != opteof)
		switch (opt) {
		case '?':
			strerr_die1x(1, usage);
			break;
		case 'f':
			file = optarg;
			break;
		case 'i':
			install = 1;
			gotit = 1;
			break;
		case 'm':
			macro = optarg;
			break;
		}

	argv += optind; argc -= optind;
	if (argc != 1) strerr_die1x(1, usage);

	fd = open_read(*argv);
	if (fd == -1)
		strerr_die4sys(1, fatal,
			       "cannot open `", *argv, "' for input: ");

	hash_init(&ht, 53);
	buffer_init(&in, buffer_unixread, fd, buf, sizeof buf);
	if (!stralloc_copyb(&line, "", 0)) nomem();

	while (buffer_get(&in, &ch, 1)) {
		if (ch == '\n') {
			parse(&line);
			if (!stralloc_copyb(&line, "", 0)) nomem();
		} else
			if (!stralloc_catb(&line, &ch, 1)) nomem();
	}

	if (!gotit)
		strerr_warn2(error, "file / macro not found", 0);

	return !gotit;
}
