#ifndef SELECT_H
#define SELECT_H

#if HASsysel
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
extern int select();
#else
#include <sys/types.h>
#include <sys/time.h>
extern int select();
#endif

#endif
