#include "stralloc.h"

int stralloc_catoct0(stralloc *sa, unsigned long u, unsigned int n) {
  unsigned int len;
  unsigned long q;
  char *s;

  len = 1;
  q = u;
  while (q > 7) { ++len; q /= 8; }
  if (len < n) len = n;

  if (!stralloc_readyplus(sa,len)) return 0;
  s = sa->s + sa->len;
  sa->len += len;
  while (len) { s[--len] = '0' + (u % 8); u /= 8; }

  return 1;
}
