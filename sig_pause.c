#include <signal.h>
#include "sig.h"
#include "hassgprm.h"

#ifdef HASsgprm
#define HASSIGPROCMASK
#else
#undef HASSIGPROCMASK
#endif

void sig_pause()
{
#ifdef HASSIGPROCMASK
  sigset_t ss;
  sigemptyset(&ss);
  sigsuspend(&ss);
#else
  sigpause(0);
#endif
}
