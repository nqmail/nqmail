#ifndef UINT32_H
#define UINT32_H

#include "hasuint32.h"

#ifdef HASuint32
typedef unsigned int uint32;
#else
typedef unsigned long uint32;
#endif

#endif
