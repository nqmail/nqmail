#ifndef MREF_H
#define MREF_H 1

#include <stddef.h>

int mref_parse(const char *, char **, size_t *, char **, size_t *);
int mref_valid(const char*);

#endif
